# Unidad C0: Recapitulación

Miquel Real

*EL dominio de las BBDD es una habilidad que nos serà imprescindible para nuestra vida laboral. Los **DBSM** son sistemas maduros y potentes de los cuales nos acercaremos en este curso a conocer y manejar sus posibilidades. Como programadores nos descargamos de la tarea de la ubicación y la consistencia de los datos a cambio de aprender una sencilla sintáxis.*

## Concepto y origen de las bases de datos
¿Qué son las bases de datos? ¿Qué problemas tratan de resolver? 
*Los datos de por si no son información, para que esta sea así debemos de de estructurarla y organizarla para un acceso y análisis eficiente. A nivel de usuario he trabajé mis primeras tablas y formularios en DBaseII de Borland y reparar y compactar los archivos de Acces de Ms.*
Definición de base de datos.
*Una base de datos es un sistema informático que es capaz de almacenar y ofrecer datos como información.*
[nostalgia?](https://psychocod3r.wordpress.com/2020/07/21/exploring-borland-dbase-iv-for-dos/)

## Sistemas de gestión de bases de datos
¿Qué es un sistema de gestión de bases de datos (DBMS)?
*Un **DBMS** es un software que gestiona cómo se guarda la información en el hardware y ofrece estos datos a las aplicaciones autorizadas que lo consulten. De forma individual o concurrente*
¿Qué características de acceso a los datos debería proporcionar?
*Un **DBSM** debe de abstraer el acceso a los datos, tanto para lectura como escritura, garantizar la integridad y persistencia, de los mismos, gestionar los permisos de acceso, efifiencia,  dar seguridad,  facilitar las búsquedas.*
Definición de DBMS.
*Es un software que resuelve las características anteriormente descritas.*

### Ejemplos de sistemas de gestión de bases de datos
¿Qué DBMS se usan a día de hoy? ¿Cuáles de ellos son software libre? ¿Cuáles de ellos siguen el modelo cliente-servidor?

* Oracle DB
Relacional, cliente-servidor, propietaria, aunque con alguna version de estudiante.
* IBM Db2
No la conocía, en las busquedas me dice, que es propietaria de IBM, jerarquica y modelo cliente-sevidor.
* SQLite
Se ejecuta y accede en el mismo equipo, en Python se crea y consulta sin necesidad de SGBD específico, es libre.
* MariaDB
Es relacional, cliente-servidor.
* SQL Server
Es relacional cliente-servidor, privativa de Microsoft.
* PostgreSQL
Es relacional, cliente-servidor y open source.
* mySQL
Es relacional, cliente-servidor y open source.

## Modelo cliente-servidor
¿Por qué es interesante que el DBMS se encuentre en un servidor? ¿Qué ventajas tiene desacoplar al DBMS del cliente? ¿En qué se basa el modelo cliente-servidor?

Ventajas :
* Acceso simultáneo a datos.
* Escalabilidad
* Procesos de gestión de hardware desvinculados de las aplicaciones clientes.
* Facilidad de mantenimiento de los datos.
* Protección de la integridad


* __Cliente__: Es el sw o equipo que requiere el servicio
* __Servidor__: Es el sw o equipo que ofrece  el servicio
* __Red__: Es el medio físico de conexion entre ambos
* __Puerto de escucha__: Es el puerto habilitado sobre la ip del serpara escuchar las peticiones del cliente
* __Petición__: es la solivitud de cliente
* __Respuesta__: es la respuesta que devuelve el servidor

## SQL
¿Qué es SQL? ¿Qué tipo de lenguaje es?
*Es un lenguaje de consulta, de alto nivel, declarativo, es también un estandard de sintaxis en búsquedas de datos pudiendose incluso usar en algunas hojas de cálculo.*

### Instrucciones de SQL
*Las instrucciones SQL son un lenguaje de muy alto nivel, cercano al natural, para hacer búsquedas extractos de datos y manipulación de  os mismos sobre la bbdd.  Las consultas SQL no sóo las podemos encontrar en BBDD, también en modernas hojas de cálculo.*

#### DDL Data Definition Language
*DDL es una categoría de SQL que se emplea para definir la estructura de una base de datos. Incluye comandos como CREATE, ALTER y DROP, que se usan para crear, modificar y eliminar tablas, índices y otras estructuras de datos.
Create, Alter, ...*
#### DML Data Manipulation Language
*DML es una categoría de SQL que se utiliza para manipular los datos almacenados en una base de datos. Incluye comandos como SELECT, INSERT, UPDATE y DELETE, que se usan para seleccionar, agregar, modificar y eliminar datos de una base de datos.*
#### DCL Data Control Language
*DCL es una categoría de SQL que se usa para controlar el acceso a los datos almacenados en una base de datos. Incluye comandos como GRANT y REVOKE, que se usan para otorgar y revocar privilegios de acceso a los usuarios de una base de datos.
Grant, Revoque*
#### TCL Transactional Control Language
*TCL es una categoría de SQL que se utiliza para controlar las transacciones en una base de datos. Incluye comandos como COMMIT y ROLLBACK, que se utilizan para confirmar o revertir una transacción en una base de datos*

[webgrafía](https://programacionfacil.org/blog/categorias-dml-ddl-tcl-dcl-y-dql-de-tipos-de-operaciones-sql/#SQL-TCL-Transaction-Control-Language)


## Bases de datos relacionales
¿Qué es una base de datos relacional? ¿Qué ventajas tiene? ¿Qué elementos la conforman?

*Una base de datos relacional es un tipo de base de datos que almacena y proporciona acceso a puntos de datos relacionados entre sí. Las bases de datos relacionales se basan en el modelo relacional, una forma intuitiva y directa de representar datos en tablas. En una base de datos relacional, cada fila en una tabla es un registro con una ID única, llamada clave. Las columnas de la tabla contienen los atributos de los datos y cada registro suele tener un valor para cada atributo, lo que simplifica la creación de relaciones entre los puntos de datos.*

* __Relación (tabla)__:  Una tabla es el átomo de la relación, conde los registros, se relacionan con los campos
* __Atributo/Campo (columna)__: Un campo es el valos que puede un dato del registro
* __Registro/Tupla (fila)__: Un registro es un conjunto de datos, una anotación en referencia a un evento o entidad


