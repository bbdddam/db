# Reto 1: Consultas básicas

Miquel Real Rausell

En este reto trabajamos con la base de datos `sanitat`, que nos viene dada en el fichero `sanitat.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

Mis ejercicios están en: https://gitlab.com/bbdddam/db

## Query 1

*Muestre los hospitales existentes (número, nombre y teléfono).*

Seleccionamos la BBDD, y ponemos los campos asociados a su tabla

```sql
USE sanitat;
SELECT HOSPITAL.HOSPITAL_COD, HOSPITAL.NOM, HOSPITAL.TELEFON
FROM HOSPITAL;
```


## Query 2

*Muestre los hospitales existentes (número, nombre y teléfono) que tengan
una letra A en la segunda posición del nombre.*

Para esta consulta usamos comodines, en SQL el comodín _ substituye a un caracter y % a un conjunto de carácteres

```sql
USE sanitat;
SELECT HOSPITAL_COD, NOM, TELEFON
FROM HOSPITAL
WHERE NOM LIKE "_A%";
```
Opción SUBSTRING(campo,índice,tamaño)
Atención: los índices en SQL empiezan en 1

```SQL
WHERE SUBSTRING(NOM,2,1)="A";
```

## Query 3

*Muestre los trabajadores (código hospital, código sala, número empleado y apellido) existentes*
```SQL
SELECT HOSPITAL_COD, SALA_COD, EMPLEAT_NO,COGNOM
FROM PLANTILLA
```

## Query 4

*Muestre los trabajadores (código hospital, código sala, número empleado y apellido) que no sean del turno de noche.*

```SQL
SELECT HOSPITAL_COD, SALA_COD, EMPLEAT_NO,COGNOM
FROM PLANTILLA
WHERE TORN != "N";
```

exixten cotras comparativas equivalentes p.e.
```SQL
WHERE TORN <> "N";
WHERE NOT TORN = "N";
...
```

## Query 5

*Muestre a los enfermos nacidos en 1960.*

```SQL
SELECT *
FROM MALALT
WHERE YEAR(DATA_NAIX)="1960";
```
Otras opciones 
```SQL
WHERE DATA_NAIX BETWEEN "1960-01-01" AND "1960-12-31"
```

## Query 5

*Muestre a los enfermos nacidos a partir del año 1960.*
```sql
SELECT *
FROM MALALT, 
WHERE YEAR(DATA_NAIX)="1960";
```

## Query 6
*Muestre a los enfermos nacidos a partir del año 1960.*

```sql
SELECT *
FROM MALALT
WHERE YEAR(DATA_NAIX)>="1960";
```

Otras opciones
```sql
WHERE DATA_NAIX BETWEEN "1960-01-01" AND CURRENT_DATE();
```

