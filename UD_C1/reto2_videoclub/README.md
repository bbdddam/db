# Reto 2: Consultas básicas II

Miquel Real Rausell
[GitLab de DDBB de Miquel Real 1er DAM](https://gitlab.com/bbdddam/db.git)

*Consultes bàsiques i senzilles, en cap d'elles hem habut de creuar taules.*

## DB "empresa"
### Query 1 
Muestre los productos (código y descripción) que comercializa la empresa.
```sql
USE empresa;
SELECT *
FROM PRODUCTE;
```
*no es precis filtrar al SELECT, ens demanen tot els camps de la taula*
### Query 2 
Muestre los productos (código y descripción) que contienen la palabra tennis en la descripción.
```sql
USE empresa;
SELECT *
FROM PRODUCTE
WHERE DESCRIPCIO LIKE '%TENNIS%';
```
*Filtrant amb WHERE i fent ús de comodins*

### Query 3
Muestre el código, nombre, área y teléfono de los clientes de la empresa.
```sql
USE empresa;
SELECT CLIENT_COD, NOM , AREA, TELEFON
FROM empresa.CLIENT;
```
*Enumerant al SELECT únicament les columnes adients.
He afegit el nom de la db a la referència CLIENT per si coincideix el nom de la taula amb alguna paraula reservada del SQL*
### Query 4
Muestre los clientes (código, nombre, ciudad) que no son del área telefónica 636.
```sql
USE empresa;
SELECT CLIENT_COD, NOM , CIUTAT
FROM empresa.CLIENT
WHERE CLIENT.AREA!=636;
```
*Enumerant al SELECT únicament les columnes adients, i filtrant amb WHERE*

### Query 5
Muestre las órdenes de compra de la tabla de pedidos (código, fechas de
orden y de envío)..
```sql
USE empresa;
SELECT COM_NUM,COM_DATA,DATA_TRAMESA
FROM COMANDA;
```
*Consulta obvia si saps valencià*

## DB "videoclub"
### Query 6 
Lista de nombres y teléfonos de los clientes.
```sql
USE videoclub;
SELECT Nom,Telefon
FROM videoclub.CLIENT;
```
*Sel·leccionant la bd videoclub, de la taula CLIENT, mostrem les columnes, Nom, Telefon.*
### Query 7
Lista de fechas e importes de las facturas.
```sql
USE videoclub;
SELECT FACTURA.Data, FACTURA.Import
FROM FACTURA;
```
*Procedint al igual que l'anterior, el client marca "Client" i "Data" com a paraules reservades, per precaució les referèncie a la propia taula FACTURA*
### Query 8
Lista de productos (descripción) facturados en la factura número 3.
```sql
USE videoclub;
SELECT Descripcio
FROM DETALLFACTURA
WHERE CodiFactura=3;
```
*El número de factura es ja la foreingkey, no necessitem fer us de la taula FACTURA per a aquesta consulta*
### Query 9
Lista de facturas ordenada de forma decreciente por importe.
```sql
USE videoclub;
SELECT * 
FROM FACTURA
ORDER BY Import DESC;
```
*ORDER BY ordena per qualsevol camp de la taula o el camp calcul·lat*
### Query 10
Lista de los actores cuyo nombre comience por X.
```sql
USE videoclub;
SELECT * 
FROM ACTOR
WHERE Nom LIKE "X%";
```
*Filtrant amb WHERE, LIKE i comodins*

### Query 11
Añadimos mas actores con X.
```sql
USE videoclub;
INSERT INTO ACTOR
VALUES (5, "Xavi");
```
*Afegir registres, camps per posició*


### Query 12
Añadir registros independientemente de la posicion
``` sql
USE videoclub;
INSERT INTO videoclub.ACTOR(Nom,CodiActor)
VALUES("Xordi", 7);
```
*Afegir registres definint l'ordre dels camps prèviament. També podriem insertar registres parcialment, sense conèixer totes les dades*

### Query 13
Eliminar registros.
```sql
USE videoclub;
DELETE FROM ACTOR
WHERE CodiActor = 7 ; -- es "Xordi"
```
*Com a defecte l'eliminació massiva esta protegida*

### Query 14
Añadir varios registros en un INSERT
```sql
USE videoclub;
INSERT INTO ACTOR
VALUES (7, "Xordi"),(6, "Ximo");
```
### Query 15
Modificar registros
```sql
UPDATE ACTOR 
SET Nom = "Jordi"
-- WHERE Nom="Xordi" (no funciona son safe update activado)
WHERE CodiActor = 7;
```
*UPDATE actualitza camps sobre els registres existents*

### Query 16

```sql
-- ¿En que años hemos dado de alta a trabajadores?
USE empresa;
SELECT DISTINCT YEAR(DATA_ALTA)
FROM EMP;
```

*Disctint actúa sobre els registres de la mateixa línea*

### Query 17
```sql
-- ¿Que categorias de empleados tengo actualmente?
USE empresa;
SELECT DISTINCT OFICI
FROM EMP;
-- ¿Cuantas categorias de empleados tengo actualmente?
USE empresa;
SELECT COUNT(DISTINCT OFICI)
FROM EMP;

```

*Disctint actúa sobre els registres de la mateixa línea*

### Query 18
```sql
-- ¿Que empleados van a comissión?
USE empresa;
SELECT *
FROM EMP
WHERE COMISSIO IS NOT NULL;
```
* IS NOT NULL localitza tots els que tinguen qualsevol valor*

### Query 19
```sql
-- Top de los 2 comisionistas 
USE empresa;
SELECT *
FROM EMP
WHERE COMISSIO IS NOT NULL
ORDER BY COMISSIO DESC
LIMIT 2;

```
*LIMIT en altres gestors de BBDD es TOP*



