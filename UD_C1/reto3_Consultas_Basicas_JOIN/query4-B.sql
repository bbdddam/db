USE videoclub;
SELECT P.Titol, A.Nom
FROM PELICULA AS P
JOIN INTERPRETADA AS I
ON  P.CodiPeli = I.CodiPeli
JOIN ACTOR AS A
ON  I.CodiActor = A.CodiActor;