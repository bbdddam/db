# Reto 3: Consultas básicas con JOIN

Miquel Real Rausell
[GitLab de DDBB de Miquel Real 1er DAM](https://gitlab.com/bbdddam/db.git)

*Crec que he deprés definitivament el concepte de INNER, LEFT, RIGHT i FULL *
*A Russafa, queda l'ultim videoclub obert d'Europa, Stromboli*

## DB "empresa"
```sql
USE videoclub;
```
*Sel·leccionem la base de dades adient*

### Query 1 
Lista todas las películas del videoclub junto al nombre de su género.
```sql
SELECT P.Codipeli, P.Titol, G.Descripcio
FROM PELICULA AS P JOIN GENERE AS G
ON P.CodiGenere = G.CodiGenere;
```
*Fem un join, per igualant el codi de génere, es prdria fer un LEFT JOIN si hi hagueren pel·licules sense génere.*
### Query 2 
Lista todas las facturas de María.
```sql
USE videoclub;
SELECT FACTURA.*, CLIENT.Nom
FROM FACTURA
JOIN CLIENT
ON FACTURA.DNI = CLIENT.DNI
WHERE CLIENT.Nom LIKE 'Maria%';
```
*Es el primer videoclub que veig que fa factures, el mes habitual es (era) que paguen mòduls*
### Query 3
Lista las películas junto a su actor principal.
```sql
USE videoclub;
SELECT P.Titol, A.Nom
FROM PELICULA AS P
JOIN ACTOR AS A
ON  P.CodiActor= A.CodiActor;
```
*Entenem que P.Codiactor es el principal, puix es una clau 1-n*

### Query 4
Lista películas junto a todos los actores que la interpretaron.
```sql
USE videoclub;
SELECT P.Titol, A.Nom
FROM PELICULA AS P
JOIN INTERPRETADA AS I
JOIN ACTOR AS A
ON  P.CodiPeli = I.CodiPeli AND I.CodiActor = A.CodiActor;
```
*Es poden fer dos JOINS uno darrere del l'altre, cadascun te el seu ON :*

```sql
USE videoclub;
SELECT P.Titol, A.Nom
FROM PELICULA AS P
JOIN INTERPRETADA AS I
ON  P.CodiPeli = I.CodiPeli
JOIN ACTOR AS A
ON  I.CodiActor = A.CodiActor;
```
*les taules intrmèdies com "INTERPRETADA" són la manera de fer una rel·lació n-n en bbdd relacionals*

### Query 5
Lista ID y nombres de las películas junto a los ID y nombres de sus segundas partes.
```sql
USE videoclub;
SELECT P1.Codipeli, P1.Titol, P2.Codipeli, P2.Titol
FROM PELICULA AS P1
JOIN PELICULA AS P2
ON  P1.Segonapart=P2.Codipeli;
```
*si la taula està relacionada amb si mateixa, farem ús dels "alias" per a identificar-les*
