-- Álbumes que tienen más de 15 canciones, junto a su artista.
SELECT Artist.Name, Title
FROM Album JOIN Artist ON Album.ArtistId =Artist.ArtistId
WHERE AlbumId IN 
(SELECT AlbumId
FROM Track
GROUP BY AlbumId
HAVING count(*) > 15) ;