-- Muestra los clientes junto con la cantidad total de dinero gastado por cada uno en compras.
SELECT CustomerId, count(*) , sum(Total)
FROM Invoice
GROUP BY CustomerId
