-- Canciones del género con más canciones.
SELECT * 
FROM Track
WHERE Track.GenreId =
	(SELECT Genre.GenreId
	FROM Genre JOIN Track USING(GenreId)
	GROUP BY GenreId
	ORDER BY COUNT(*) DESC
	LIMIT 1);