# Reto 2.3: Consultas con subconsultas II
**[Bases de Datos] Unidad Didáctica 2: DML - Consultas Avanzadas**

Para este reto, volveremos a usar la base de datos Chinook (más información en el Reto 2.1).

![Diagrama relacional de Chinook (fuente: github.com/lerocha/chinook-database).](https://github.com/lerocha/chinook-database/assets/135025/cea7a05a-5c36-40cd-84c7-488307a123f4)


Tras cargar esta base de datos en tu SGBD, realiza las siguientes consultas:

## Subconsultas Escalares (Scalar Subqueries)
Estas subconsultas devuelven un solo valor, por lo general, se utilizan en contextos donde se espera un solo valor, como parte de una condición `WHERE`, `SELECT`, `HAVING`, etc.
Ejemplo:

_Obtener una lista de empleados que ganan más que el salario medio de la empresa. [1]_

```sql
SELECT
  lastname,
  firstname,
  salary
FROM employee
WHERE salary > (SELECT avg(salary)
                FROM employee)
```

### Consulta 1
Obtener las canciones con una duración superior a la media.

```sql
SELECT *
FROM Track
WHERE Milliseconds > (SELECT avg(Milliseconds) FROM Track)
```

Muy parecidoa al ejemplo, el valor de la media se obrtiene con una subconsulta *escalar*


### Consulta 2
Listar las 5 últimas facturas del cliente cuyo email es "emma_jones@hotmail.com".

```sql
SELECT *
FROM Invoice
WHERE CustomerId =(SELECT CustomerId
FROM Customer
WHERE Email = "emma_jones@hotmail.com")
ORDER BY InvoiceDate DESC
LIMIT 5;
```

Este tipo de consultas también se resuelven fácilmente con JOIN, pero quizà quede mas clara o editable en segun que contextos. 
Por ejemplo si ya has desarrollado una consulta que obtienen un dato, la "reusas" para realizar otra a partir de la obtenida.

A nivel de prestaciones, se supoe que SQL es un lenguaje de *muy alto nivel* y no debe de afectar en su eficiencia.

## Subconsultas de varias filas

Diferenciamos dos tipos:

1. Subconsultas que devuelven una columna con múltiples filas (es decir, una lista de valores). Suelen incluirse en la cláusula `WHERE` para filtrar los resultados de la consulta principal. En este caso, suelen utilizarse con operadores como `IN`, `NOT IN`, `ANY`, `ALL`, `EXISTS` o `NOT EXISTS`.
2. Subconsultas que devuelven múltiples columnas con múltiples filas (es decir, tablas). Se comportan como una tabla temporal y se utilizan en lugares donde se espera una tabla, como en una cláusula `FROM`. [2]

### Consulta 3
Mostrar las listas de reproducción en las que hay canciones de reggae.

```sql
SELECT *
FROM Playlist
WHERE PlaylistId IN
(SELECT DISTINCT PlaylistId 
FROM PlaylistTrack
WHERE TrackId IN (SELECT TrackId FROM Track WHERE GenreId = (SELECT GenreId FROM Genre WHERE Name = "reggae")));
```

[asi va esto](https://es.wikipedia.org/wiki/Matrioshka#/media/Archivo:Russian-Matroshka_no_bg.jpg)

### Consulta 4
Obtener la información de los clientes que han realizado compras superiores a 20€.

```sql
SELECT DISTINCT * 
FROM Customer
WHERE CustomerId IN
(SELECT CustomerId
FROM Invoice
WHERE Total >20 );
```

Si no pusiesemos *DISTINCT* y un cliente hubiese hecho varias compras de total superior a 20 , aparecería varias veces

### Consulta 5
Álbumes que tienen más de 15 canciones, junto a su artista.

```sql
SELECT Artist.Name, Title
FROM Album JOIN Artist ON Album.ArtistId =Artist.ArtistId
WHERE AlbumId IN 
(SELECT AlbumId
FROM Track
GROUP BY AlbumId
HAVING count(*) > 15) ;
```

Usamos subconsultas y join


### Consulta 6
Obtener los álbumes con un número de canciones superiores a la media.

Primero hacemos la consulta que averigua la media de canciones de un album:

```sql
SELECT SUM(Milliseconds) AS sumas 
FROM Track
GROUP BY AlbumId
```

esto nos genera una tabla nueva, con una columna que es el munero de canciones de cada album

A esta columna le damos nombre y averiguamos la media

```sql
SELECT AVG(album_tracks) 
FROM (
    SELECT COUNT(*) AS album_tracks
    FROM Track
    GROUP BY AlbumId
) as tabla;
``` 

Entonces la mombramos y 


### Consulta 7
Obtener los álbumes con una duración total superior a la media.

En un principio, obtenemos una tabla con la suma de los ms de las canciones de cada album:

 ```sql
SELECT SUM(Milliseconds) AS sumas 
FROM Track
GROUP BY AlbumId
 ```

Al igual que en la anterior, averiguamos la media

 ```sql
SELECT Title, AlbumId, SUM(Milliseconds)
FROM Track JOIN Album USING(AlbumId)
GROUP BY AlbumId 
HAVING SUM(Milliseconds) >
	(SELECT avg(Duracion) AS Media
	FROM 	(SELECT AlbumId, SUM(Milliseconds) AS Duracion
			FROM Track
			GROUP BY AlbumId) AS x);
 ```

### Consulta 8
Canciones del género con más canciones.

 ```sql
SELECT * 
FROM Track
WHERE Track.GenreId =
	(SELECT Genre.GenreId
	FROM Genre JOIN Track USING(GenreId)
	GROUP BY GenreId
	ORDER BY COUNT(*) DESC
	LIMIT 1);
 ```

### Consulta 9
Canciones de la _playlist_ con más canciones.

Calculamos el id de la lista mayor:

```sql
SELECT PlaylistId 
FROM  Playlist JOIN PlaylistTrack USING(PlaylistId)
GROUP BY PlaylistId
ORDER BY COUNT(*) DESC
LIMIT 1;
```

y lo usamos paraq filtrar las listas:

```sql
SELECT *
FROM PlaylistTrack
WHERE PlaylistId =
	(SELECT PlaylistId 
	FROM  Playlist JOIN PlaylistTrack USING(PlaylistId)
	GROUP BY PlaylistId
	ORDER BY COUNT(*) DESC
	LIMIT 1);
```

si queremos los nombres, `JOIN`

```sql
SELECT Track.Name
FROM PlaylistTrack JOIN Track USING(TrackId)
WHERE PlaylistId =(SELECT PlaylistId 
	                 FROM  Playlist JOIN PlaylistTrack USING(PlaylistId)
	                 GROUP BY PlaylistId
	                 ORDER BY COUNT(*) DESC
	                 LIMIT 1);
```

## Subconsultas Correlacionadas (Correlated Subqueries):
Son subconsultas en las que la subconsulta interna depende de la consulta externa. Esto significa que la subconsulta se ejecuta una vez por cada fila devuelta por la consulta externa, suponiendo una gran carga computacional.
Ejemplo:

_Supongamos que queremos encontrar a todos los empleados con un salario superior al promedio de su departamento. [1]_

```sql
SELECT
  lastname,
  firstname,
  salary
FROM employee e1
WHERE e1.salary > (SELECT avg(salary)
                   FROM employee e2
                   WHERE e2.dept_id = e1.dept_id)
```

La principal diferencia entre una subconsulta correlacionada en SQL y una subconsulta simple es que las subconsultas correlacionadas hacen referencia a columnas de la tabla externa. En el ejemplo anterior, `e1.dept_id` es una referencia a la tabla de la subconsulta externa. [1]


### Consulta 10
Muestra los clientes junto con la cantidad total de dinero gastado por cada uno en compras.
Para esta consulta no es necesario hacerla con subconsultas:

```sql
SELECT CustomerId, count(*) AS NumeroFacturas , sum(Total)
FROM Invoice
GROUP BY CustomerId
```

para hacerlo con subconsultas, ponemos la subconsulta en el `SELECT`

```sql
SELECT FirstName,
    LastName,
    City,
    (
        SELECT sum(Total)
        FROM Invoice
        WHERE CustomerId = c.CustomerId
    ) AS Total_Compras
FROM Customer c
```

### Consulta 11
Obtener empleados y el número de clientes a los que sirve cada uno de ellos.
Por la Foreing Key sabemos que `SupportRepId` apunta a empleado

esta como la otra la podriamos hacer sin subconsulta:

```sql
SELECT Employee.FirstName, Employee.LastName, count(*)
FROM Chinook.Customer JOIN Employee ON EmployeeId=SupportRepId
GROUP BY SupportRepId;
```

Pero vamos a hacerlo difícil!
Dentro de un listado básico de empleados añadimos la subconsulta de los clientes que le tienen como asignado

```sql
SELECT FirstName,
    LastName,
    (
        SELECT count(*)
        FROM Customer
        WHERE SupportRepId = E.EmployeeId
    ) AS Asignados
FROM Employee AS E
```

### Consulta 12
Ventas totales de cada empleado.
Vamos a Hacerla  con JOIN para practicar:

```sql
SELECT Employee.FirstName,
    Employee.LastName,
    SUM(Total)
FROM Invoice
    JOIN Customer USING(CustomerId)
    right JOIN Employee ON EmployeeId = SupportRepId
```

y ahora vamos al lio....

las sumas de las facturas de los clientes que tienen asignado un gestor x
(refrita de la consulta 10)

```sql
SELECT FirstName,
    LastName,
    (
        SELECT SUM(Tot)
        FROM (
                SELECT (
                        SELECT sum(Total)
                        FROM Invoice
                        WHERE CustomerId = c.CustomerId
                            and SupportRepId = E.EmployeeId
                    ) AS Tot
                FROM Customer c
            ) AS PEPE
    ) As ventas
FROM Employee AS E
```



### Consulta 13
Álbumes junto al número de canciones en cada uno.

con JOIN :

```sql
SELECT Album.Title, count(*)
FROM Album JOIN Track USING(AlbumId)
GROUP BY AlbumId
```

Ahora veremos

Para contar el numero de canciones de un album X

```sql
SELECT Album.Title, count(*)
FROM Album JOIN Track USING(AlbumId)
WHERE AlbumId= X
```

asi se queda:

```sql
SELECT bum.Title,
    (
        SELECT count(*)
        FROM Album AS A
            JOIN Track USING(AlbumId)
        WHERE AlbumId = bum.AlbumId
    ) as AL
FROM Album AS bum
```

### Consulta 14
Obtener el nombre del álbum más reciente de cada artista. Pista: los álbumes más antiguos tienen un ID más bajo.

Albumes de mayor Id, por artista
```sql
SELECT max(Album.AlbumId)
FROM Album
GROUP BY ArtistId
```
Usándolo como subconsulta:


```sql
SELECT Artist.name,
    Album.Title
FROM Artist
    JOIN Album USING(ArtistId)
WHERE AlbumId IN (
        SELECT max(Album.AlbumId)
        FROM Album
        GROUP BY ArtistId
    )
```

## Referencias
- [1] https://learnsql.es/blog/subconsulta-correlacionada-en-sql-una-guia-para-principiantes/
- [2] https://learnsql.es/blog/cuales-son-los-diferentes-tipos-de-subconsultas-sql/


<!--
HAVING ??
GROUP BY ??
-->
