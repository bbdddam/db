-- Mostrar las listas de reproducción en las que hay canciones de reggae.
SELECT *
FROM Playlist
WHERE PlaylistId IN
(SELECT DISTINCT PlaylistId 
FROM PlaylistTrack
WHERE TrackId IN (SELECT TrackId FROM Track WHERE GenreId = (SELECT GenreId FROM Genre WHERE Name = "reggae")));