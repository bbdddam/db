-- Obtener los álbumes con una duración total superior a la media.
SELECT Title, AlbumId, SUM(Milliseconds)
FROM Track JOIN Album USING(AlbumId)
GROUP BY AlbumId 
HAVING SUM(Milliseconds) >
	(SELECT avg(Duracion) AS Media
	FROM 	(SELECT AlbumId, SUM(Milliseconds) AS Duracion
			FROM Track
			GROUP BY AlbumId) AS x);
