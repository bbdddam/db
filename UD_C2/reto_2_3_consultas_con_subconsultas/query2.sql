-- Listar las 5 últimas facturas del cliente cuyo email es "emma_jones@hotmail.com".
SELECT *
FROM Invoice
WHERE CustomerId =(SELECT CustomerId
FROM Customer
WHERE Email = "emma_jones@hotmail.com")
ORDER BY InvoiceDate DESC
LIMIT 5;