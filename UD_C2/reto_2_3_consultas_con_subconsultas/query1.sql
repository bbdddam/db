USE Chinook;
-- Obtener las canciones con una duración superior a la media.
SELECT *
FROM Track
WHERE Milliseconds > (SELECT avg(Milliseconds) FROM Track)
