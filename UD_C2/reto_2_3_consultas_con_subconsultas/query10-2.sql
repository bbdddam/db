-- Muestra los clientes junto con la cantidad total de dinero gastado por cada uno en compras.
SELECT FirstName,
	LastName,
	City,
	(
		SELECT sum(Total)
		FROM Invoice
		WHERE CustomerId = c.CustomerId
	) AS Total_Compras
FROM Customer c