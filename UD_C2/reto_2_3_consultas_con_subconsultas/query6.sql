-- Obtener los álbumes con un número de canciones superiores a la media.
SELECT Artist.Name, Title
FROM Album JOIN Artist ON Album.ArtistId =Artist.ArtistId
WHERE AlbumId IN 
(SELECT AlbumId AS mayores
FROM Track
GROUP BY AlbumId
HAVING count(*) > (
SELECT avg(numTracks) AS media
FROM (SELECT COUNT(*) as numTracks
FROM Track
GROUP BY AlbumId) AS trackXakbum)) ;
