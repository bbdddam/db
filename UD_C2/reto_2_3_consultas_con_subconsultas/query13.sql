USE Chinook;
SELECT bum.Title,
    (
        SELECT count(*)
        FROM Album AS A
            JOIN Track USING(AlbumId)
        WHERE AlbumId = bum.AlbumId
    ) as AL
FROM Album AS bum