import clases.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class responde2 {
    public static void main(String[] args) {

        final int hor = 180, ver = 38;
        List<String[]> preguntes = Accesdb.lligTaula("PREGUNTA");
        List<String[]> respostes = Accesdb.lligTaula("RESPUESTA");
        Map<String, String> respostesUsuari = new HashMap<>();
        Map<String, Integer> yRespostesLlargues = new HashMap<>();

        // calcula pagines i ompli respostes buides
        List<String> marcaPg = new ArrayList<>();
        int cursor = 1, cursorEnunciat = 1;

        marcaPg.add(preguntes.get(0)[0]);
        for (String[] pre : preguntes) {
            if (pre[2]!=null && pre[2].contains(".")){
                String preg = pre[2].split("\\.")[0];
                String opcio = pre[2].split("\\.")[1];
                respostesUsuari.put(preg,opcio);
            }
            if (cursor > ver - 5) {
                cursor = 1 + cursor - cursorEnunciat;
                marcaPg.add(pre[0]);
            }
            cursorEnunciat = cursor;
            cursor++;
            boolean senseOpcions = true;
            for (String[] rsp : respostes) {
                if (rsp[0].startsWith(pre[0] + ".")) {
                    cursor++;
                    senseOpcions = false;
                }
            }
            if (senseOpcions) {
                String[] res_rellenar = new String[2];
                res_rellenar[0] = pre[0] + ".x";
                res_rellenar[1] = "-------";
                Accesdb.agrega("RESPUESTA",
                        new String[] { "IdRespuesta", res_rellenar[0], "respuesta", "No contestada." });
                respostes.add(res_rellenar);
                cursor++;
            }

        }
        marcaPg.add("eof");
        int pgActual = 0;
        String entrada = "";
        List<String> validOpts = new ArrayList<>();
        menuopts(validOpts);

        Pantalla p = new Pantalla(hor, ver);
        while (!entrada.toUpperCase().equals("FIN")) {
            p.setUltcol('w');
            p.marc();
            p.situa(3, 0, "CONTESTEITOR 0.0.1", 'g');
            p.situa(hor - 40, 0, "CONTESTADES:(" + respostesUsuari.size() + "/" + preguntes.size() + ")", 'c');
            p.situa(hor - 10, 0, "PÀG (" + (pgActual + 1) + "/" + (marcaPg.size() - 1) + ")", 'g');
            p.situa(hor - 85, ver - 1,
                    "'n.x'-> tria resposta 'Q' -> puja pag. 'A' -> baixa pag. 'FIN' -> finalitza", 'g');
            p.situa(hor - 84, ver - 1,"n.x",'c');
            p.situa(hor - 62, ver - 1,"Q",'c');
            p.situa(hor - 45, ver - 1,"A",'c');
            p.situa(hor - 27, ver - 1,"FIN",'c');
            p.setCursor(2, 1);
            boolean enPg = false;
            for (String[] pr : preguntes) {
                if (pr[0].equals(marcaPg.get(pgActual)))
                    enPg = true;
                if (pr[0].equals(marcaPg.get(pgActual + 1)))
                    enPg = false;
                if (enPg) {
                    p.situa(pr[0] + " : " + pr[1], 'w');
                    for (String[] rsp : respostes) {
                        if (rsp[0].startsWith(pr[0] + ".")) {
                            validOpts.add(rsp[0]);
                            if (rsp[0].contains(".x")) {
                                yRespostesLlargues.put(rsp[0],p.getCursorY());                          
                            }
                            String opcion = "";
                            opcion = rsp[0].split("\\.")[1];
                            char color = (respostesUsuari.getOrDefault(pr[0], "@@").equals(opcion)) ? 'w' : 'n';
                            p.situa("     ." + opcion + " - " + rsp[1], color);
                        }

                    }
                }
            }
            p.situa(hor - 50, ver - 3, " ", 'c');
            entrada = p.getString("-> ", validOpts);
            if (entrada.toUpperCase().equals("Q") && pgActual > 0)
                pgActual--;
            else if (entrada.toUpperCase().equals("A") && pgActual < marcaPg.size() - 2)
                pgActual++;
            else if (entrada.contains(".")) {
                String preg = entrada.split("\\.")[0];

                String opcio = entrada.split("\\.")[1];
                respostesUsuari.put(preg, opcio);
                if (opcio.equals("x")) {
                    p.situa(12, yRespostesLlargues.get(entrada), " ".repeat(hor - 14));
                    p.lineUp();
                    String texteresposta = p.getString();
                    Accesdb.modifica(String.format("UPDATE RESPUESTA SET respuesta ='%s' WHERE IdRespuesta='%s';",
                            texteresposta, entrada));
                    respostes.removeIf(resposta -> resposta[0].equals(preg + "." + opcio));
                    respostes.add(new String[] { entrada, texteresposta });
                }
            }

            p.borra();
            validOpts.clear();
            menuopts(validOpts);
        }
        for (String IdPregunta : respostesUsuari.keySet()) {
            Accesdb.modifica(String.format("UPDATE PREGUNTA SET IdRespuesta='%s' WHERE IdPregunta=%s",
                    IdPregunta+"."+respostesUsuari.get(IdPregunta), IdPregunta));
        }
        // Accesdb.modifica("DELETE FROM RESPUESTA WHERE IdRespuesta LIKE '%.x';");
    }

    private static void menuopts(List<String> validOpts) {
        validOpts.add("fin");
        validOpts.add("FIN");
        validOpts.add("q"); // puja pg
        validOpts.add("Q"); // puja pg
        validOpts.add("a"); // baixa pg
        validOpts.add("A"); // baixa pg
    }
}
