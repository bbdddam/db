# CONTESTEITOR 0.0.1
## Java acces db practice

This is a prectice linux terminal utility, that alows user to answer a test stored in a ddbb in a fixed questions-options schema.

In queries folder you have the SQL query to create the default schema, and a query to fillup with dummie data.

The utility allows choose a single option between the linked with IdRespuesta first calue before . (dot)

Besides you can fill up a option if there is none for a question. Thoose options Id will be stored in format #question(dot)x


*Personally, I think that the chosen scheme can be greatly improved, with an intermediate question-answer table, but understanding that Cristian did not want to make it more complicated for us on the day of the exam.*

In clases folder you can edit Accesdb.java file options to acces your DBMS 

To compile java project in some configurations you can use:

``` bash
javac ./clases/*.java ./*.java
```

To execute from command line use 

``` bash
java -cp ./lib/mysql-connector-j-8.3.0.jar:. ./responde2.java
```
to set the path for the propper db connector.

or use ```contesta.sh```

