#Muestra el número de canciones de cada género (deberá mostrarse el nombre del género).

SELECT Genre.Name AS Género,
       COUNT(*) AS "Número de canciones"
FROM Track
JOIN Genre ON Track.GenreId = Genre.GenreId
GROUP BY Genre.Name;
