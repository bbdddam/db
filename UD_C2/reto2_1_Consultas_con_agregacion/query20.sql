#Muestra los países en los que tenemos al menos 5 clientes.
SELECT Country, count(Country) AS "Numero de clientes"
FROM Customer
GROUP BY Country
HAVING COUNT(*) >= 5;
