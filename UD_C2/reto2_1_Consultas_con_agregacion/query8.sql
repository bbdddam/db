# empleados mars jovenes y su supervisor, si tienen
SELECT e1.FirstName AS "Empleado",
       e1.LastName AS "Apellido Empleado",
       e2.FirstName AS "Supervisor",
       e2.LastName AS "Apellido Supervisor"
FROM Employee AS e1
right JOIN Employee AS e2 ON e1.ReportsTo = e2.EmployeeId
ORDER BY e1.BirthDate DESC
LIMIT 15;

