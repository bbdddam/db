#Muestra el importe medio, mínimo y máximo de cada factura. 
SELECT min(Total) AS Mínima, max(Total) AS Máxima, avg(Total) AS Media
FROM Invoice;

#por meses
SELECT  monthname(((InvoiceDate))) AS mes, min(Total) AS Mínima, max(Total) AS Máxima, avg(Total) AS Media
FROM Invoice
GROUP BY mes;