#Lista los 6 álbumes que acumulan más compras. (con artista)
SELECT Album.Title, Artist.Name, COUNT(Album.Title) as Ventas 
FROM  InvoiceLine AS venta
JOIN Track ON venta.TrackId=Track.TrackId
JOIN Album ON Track.AlbumId=Album.AlbumId
JOIN Artist ON Album.ArtistId=Artist.ArtistId
GROUP BY Album.Title, Artist.ArtistId
ORDER BY Ventas DESC
LIMIT 6;

