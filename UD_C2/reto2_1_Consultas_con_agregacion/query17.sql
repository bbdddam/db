#Muestra los álbumes ordenados por el número de canciones que tiene cada uno.

SELECT Album.Title AS Título , count(*) as Pistas
FROM Track  JOIN Album
ON Track.AlbumId=Album.AlbumId
GROUP BY Album.AlbumId
ORDER BY Pistas DESC;