# Muestra qué clientes han realizado compras por valores superiores a 10€, ordenados por apellido.
USE Chinook;
SELECT DISTINCT Customer.FirstName,
       Customer.LastName
FROM Customer
JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId
WHERE Invoice.Total > 10
ORDER BY Customer.LastName;

