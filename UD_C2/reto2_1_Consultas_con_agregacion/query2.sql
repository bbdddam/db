#facturas primer trimestre de este año
SELECT *
FROM Invoice
WHERE YEAR(InvoiceDate) = YEAR(NOW()) AND
      MONTH(InvoiceDate) <= 3;