# Muestra artistas y sus albumes
SELECT Artist.Name AS Artista, Album.Title AS Album
FROM Artist
JOIN Album ON Artist.ArtistId = Album.ArtistId
ORDER BY Artista; #mejor ordenados
