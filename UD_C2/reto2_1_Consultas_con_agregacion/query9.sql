SELECT Invoice.InvoiceDate AS FFactura,
       CONCAT(Customer.FirstName ,' ', Customer.LastName) AS Nombre,
       Customer.Address AS DirecciónFact,
       Customer.PostalCode AS CP,
       Customer.Country AS Pais,
       Invoice.Total  AS Cantidad
FROM Invoice
JOIN Customer ON Invoice.CustomerId = Customer.CustomerId
WHERE Customer.City = 'Berlin';