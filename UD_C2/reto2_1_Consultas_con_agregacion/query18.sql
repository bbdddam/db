SELECT Genre.Name,COUNT(Genre.Name) as Ventas
FROM  InvoiceLine AS venta
JOIN Track ON venta.TrackId=Track.TrackId
JOIN Genre ON Track.GenreId=Genre.GenreId
GROUP BY Genre.GenreId
ORDER BY Ventas DESC;