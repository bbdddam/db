SELECT Playlist.PlaylistId,
       Playlist.Name AS Nombre,
       Track.TrackId,
       Track.Name AS Pista,
       Album.Title AS Título,
       CONCAT(FLOOR(Track.Milliseconds / 60000), ':', LPAD(FLOOR((Track.Milliseconds % 60000) / 1000), 2, '0')) AS Duracion
FROM Playlist
JOIN PlaylistTrack ON Playlist.PlaylistId = PlaylistTrack.PlaylistId
JOIN Track ON PlaylistTrack.TrackId = Track.TrackId
JOIN Album ON Track.AlbumId = Album.AlbumId
WHERE Playlist.Name LIKE 'C%'
ORDER BY Album.Title, Track.Milliseconds;