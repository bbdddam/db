SELECT Playlist.PlaylistId,
       Playlist.Name AS Nombre,
       Track.TrackId AS id, 
       Track.Name AS Pista,
       Album.Title AS Título,
       Track.Milliseconds/1000 AS Duración
FROM Playlist
JOIN PlaylistTrack ON Playlist.PlaylistId = PlaylistTrack.PlaylistId
JOIN Track ON PlaylistTrack.TrackId = Track.TrackId
JOIN Album ON Track.AlbumId = Album.AlbumId
WHERE Playlist.Name LIKE 'C%'
ORDER BY Album.Title, Track.Milliseconds;