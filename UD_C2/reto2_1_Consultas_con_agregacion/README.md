# Reto 2.1: Consultas con agregación
Miquel Real Rausell
[GitLab de DDBB de Miquel Real 1er DAM](https://gitlab.com/bbdddam/db.git)

## DDBB Chinook

![Esquema ddbb Chinook](esquema.png)


## Consultas de una tabla

### Query 1 
Encuentra todos los clientes de Francia.
```sql
SELECT *
FROM Customer
WHERE Country = 'France';
```

En este caso no es necesario ninguna agregación, pues Customer contiene toda la informacion de la localidad ciudad y pais. No pienso que sea buena práctica pues al menos los paises (y quizá ambien las ciudades y calles) deberian de tener su propia tabla, e ir relacionadas


### Query 2 
Muestra las facturas del primer trimestre de este año.
```sql
SELECT *
FROM Invoice
WHERE YEAR(InvoiceDate) = YEAR(NOW()) AND
      MONTH(InvoiceDate) <= 3;
```

también se puede usar 

```sql
MONTH(InvoiceDate) in (1,2,3);
```

### Query 3 
Muestra todas las canciones compuestas por AC/DC.
```sql
SELECT *
FROM Track
WHERE Composer = 'AC/DC' 
```

sin embargo si quisiesemos saber las que interpretaron, hay que recurrir al album y a al artista

```sql
SELECT Track.*
FROM Track
JOIN Album ON Track.AlbumId = Album.AlbumId
JOIN Artist ON Album.ArtistId = Artist.ArtistId
WHERE Artist.Name = 'AC/DC';
```

### Query 4 
Muestra las 10 canciones que más tamaño ocupan.

```sql
SELECT *
FROM Track
ORDER BY Bytes DESC
LIMIT 10; 
```
easy

### Query 5 
Muestra el nombre de aquellos países en los que tenemos clientes.

```sql
SELECT DISTINCT Country
FROM Customer;
```
lo más dificil es escribir distinct

### Query 6 
Muestra todos los géneros musicales.

```sql
SELECT DISTINCT Name as Género
FROM Genre;
```
le cambiamos el título a la columna para que quede mas mono

## Consultas de múltiples tablas

### Query 7 
Muestra todos los artistas junto a sus álbumes.

```sql
SELECT Artist.Name AS Artista, Album.Title AS Album
FROM Artist
JOIN Album ON Artist.ArtistId = Album.ArtistId;
ORDER BY Artista;
```
ordenados por artista sale mejor


### Query 8 
Muestra los nombres de los 15 empleados más jóvenes junto a los nombres de
sus supervisores, si los tienen.

```sql
SELECT e1.FirstName AS "Empleado",
       e1.LastName AS "Apellido Empleado",
       e2.FirstName AS "Supervisor",
       e2.LastName AS "Apellido Supervisor"
FROM Employee AS e1
LEFT JOIN Employee AS e2 ON e1.ReportsTo = e2.EmployeeId
ORDER BY e1.BirthDate DESC
LIMIT 15;
```
para que salgan los empleados con o sin supervisor hacemos un LEFT JOIN, con JOIN salen solo los que tienen supervisor


### Query 9 
Muestra todas las facturas de los clientes berlineses. Deberán mostrarse las columnas: fecha de la factura, nombre completo del cliente, dirección de facturación,
código postal, país, importe (en este orden).

```sql
SELECT Invoice.InvoiceDate AS FFactura,
       CONCAT(Customer.FirstName ,' ', Customer.LastName) AS Nombre,
       Customer.Address AS DirecciónFact,
       Customer.PostalCode AS CP,
       Customer.Country AS Pais,
       Invoice.Total  AS Cantidad
FROM Invoice
JOIN Customer ON Invoice.CustomerId = Customer.CustomerId
WHERE Customer.City = 'Berlin';

```
En Mysql va bien, pero en otros SGBD `CONCAT()` puede ser `+` p.e. SQL Server o `||` en SQLite


### Query 10 
Muestra las listas de reproducción cuyo nombre comienza por C, junto a todas
sus canciones, ordenadas por álbum y por duración.


```sql
SELECT Playlist.PlaylistId,
       Playlist.Name AS Nombre,
       Track.TrackId,
       Track.Name AS Pista,
       Album.Title AS Título,
       Track.Milliseconds AS Duración
FROM Playlist
JOIN PlaylistTrack ON Playlist.PlaylistId = PlaylistTrack.PlaylistId
JOIN Track ON PlaylistTrack.TrackId = Track.TrackId
JOIN Album ON Track.AlbumId = Album.AlbumId
WHERE Playlist.Name LIKE 'C%'
ORDER BY Album.Title, Track.Milliseconds;

```
La durasción en ms es muy fea , podemos mejorarlo

```sql
CONCAT(FLOOR(Track.Milliseconds / 60000), ':', LPAD(FLOOR((Track.Milliseconds % 60000) / 1000), 2, '0')) AS Duración
```

### Query 11 
Muestra qué clientes han realizado compras por valores superiores a 10€, ordenados por apellido.

```sql
SELECT DISTINCT Customer.FirstName, Customer.LastName
FROM Customer
JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId
WHERE Invoice.Total > 10
ORDER BY Customer.LastName;
```

## Consultas con funciones de agregación

### Query 12
Muestra el importe medio, mínimo y máximo de cada factura.

```sql
SELECT min(Total) AS Mínima, max(Total) AS Máxima, avg(Total) AS Media
FROM Invoice
```
por meses seria asi:
 
```sql
SELECT  monthname(InvoiceDate) AS mes, min(Total) AS Mínima, max(Total) AS Máxima, avg(Total) AS Media
FROM Invoice
GROUP BY mes;
```

### Query 13
Muestra el número total de artistas.

```sql
SELECT count(*)
FROM Artist;
```
Al no haber finciones de agregacion va a devolver solo una linea, las funciones actuan sobre el total de los registro

### Query 14
Muestra el número de canciones del álbum “Out Of Time”.

```sql
SELECT Album.Title ,count(*)
FROM Track  JOIN Album
ON Track.AlbumId=Album.AlbumId
GROUP BY Album.AlbumId
HAVING Album.Title = 'Out Of Time'
```
Las funciones de Agregacion operan sobre la lista completa o los grupos definidos con GROUP BY

HAVING aplica condiciones sobre agregaciones, como SUM, COUNT, etc 

**NOTA: GROUP BY -> ~~WHERE~~  HAVING **


### Query 15

Muestra el número de países donde tenemos clientes.
```sql
SELECT COUNT(DISTINCT Country)
FROM Customer
```
COUNT+DISTINCT "es bien" para contar

### Query 16

Muestra el número de canciones de cada género (deberá mostrarse el nombre del género).

```sql
SELECT Genre.Name AS Género,
       COUNT(*) AS "Número de canciones"
FROM Track
JOIN Genre ON Track.GenreId = Genre.GenreId
GROUP BY Genre.Name;
```

### Query 17
Muestra los álbumes ordenados por el número de canciones que tiene cada uno.

```sql
SELECT Album.Title AS Título , count(*) as Pistas
FROM Track  JOIN Album
ON Track.AlbumId=Album.AlbumId
GROUP BY Album.AlbumId
ORDER BY Pistas DESC;
```

### Query 18
Encuentra los géneros musicales más populares (los más comprados).

```sql
SELECT Genre.Name,COUNT(Genre.Name) as Ventas
FROM  InvoiceLine AS venta
JOIN Track ON venta.TrackId=Track.TrackId
JOIN Genre ON Track.GenreId=Genre.GenreId
GROUP BY Genre.GenreId
ORDER BY Ventas DESC;
```
Haciendo esta consulta nos damos cuenta que el campo `Quantity` es irrelevante para la venta a particulares. No se puede comprar una canción (un derecho) dos veces por una persona y menos en la misma factura. 
De hecho la siguiente consulta devuelve **1**

```sql
SELECT DISTINCT Quantity FROM InvoiceLine
```

si aún así queremos sumar la columna `Quantity` nos devuelve el mismo resultado

```sql
SELECT Genre.Name, SUM(InvoiceLine.Quantity) AS Ventas
FROM  InvoiceLine 
JOIN Track ON InvoiceLine.TrackId=Track.TrackId
JOIN Genre ON Track.GenreId=Genre.GenreId
GROUP BY Genre.GenreId
ORDER BY Ventas DESC;
```


### Query 19
Lista los 6 álbumes que acumulan más compras.

```sql
SELECT Album.Title,COUNT(Album.AlbumId) as Ventas 
FROM  InvoiceLine AS venta
JOIN Track ON venta.TrackId=Track.TrackId
JOIN Album ON Track.AlbumId=Album.AlbumId
GROUP BY Album.AlbumId
ORDER BY Ventas DESC
LIMIT 6;
```

si queremos saber los Artistas;
```sql
SELECT Album.Title, Artist.Name, COUNT(Album.Title) as Ventas 
FROM  InvoiceLine AS venta
JOIN Track ON venta.TrackId=Track.TrackId
JOIN Album ON Track.AlbumId=Album.AlbumId
JOIN Artist ON Album.ArtistId=Artist.ArtistId
GROUP BY Album.Title, Artist.ArtistId
ORDER BY Ventas DESC
LIMIT 6;
```
### Query 20
Muestra los países en los que tenemos al menos 5 clientes.

```sql
SELECT Country,
       COUNT(*) AS "Numero de clientes"
FROM Customer
GROUP BY Country
HAVING COUNT(*) >= 5;
```

**No t'oblides!: GROUP BY -> ~~WHERE~~  HAVING **


