
# Reto 3.2 DCL Control de acceso

# Acceso a mysql desde terminal

EJERCICIO: CREAR UN USUARIO PARA QUE EN CHINOOK NO TENGA ACCESO COMPLETO A LA bbdd
PODEMOS AÑADIR PERMISOS POR GRUPOS DE USUARIOS
O AÑADI PERMISOS POR USUARIO Y PROCEDENCIA
TAMBIEN USAR WILCARDS PARA AÑADIR PRIVILEGIOS
los usuarios pueden tener roles: al rol se le añaden privilegios, y los roles se añaden a los usuarios
HACER UNA PRÁCTICA CON :
GRANT /REVOKE 

FKLUSH PRIVILEGES;

-SELECT 
-INSERT
-EXECUTE

crear usuario
modificar usuario
ALTER USE ivan IDENTIFIED BY nueva;
renombrar usuario
+tener en cuenta su ubicación
asignar permisos
revocar permisos

https://wiki.cifprodolfoucha.es/index.php?title=Mysql_Gesti%C3%B3n_de_permisos



crear rol
dar permisos a rol
asignar rol a ususario
GRANT rol TO 'ususario'@'%';
*tanbien se pueden asignar roles a roles, heredando permisos GRANT

activar rol de ususario

ALTER USER "ivan" ACCOUNT LOCK;
ALTER USER "ivan" ACCOUNT UNLOCK;
esto bloquea a ivan, pero si esta conectado no lo eha

SELECT id FROM information_schema.processlist WHERE user = 'ivan';



INVEETIGAR
-QUE PASA SI BORRO UN USUARIO
-COMO PUEDO ACTUALIZAR LA CONTRASEÑA DE UN USUARIO
-BLOQUEAR UN USUARTIO CON `ALTER USE LOCK/UNLOCK`
`CREATE USER ``ALTER USER` `DROP USER`
`GRANT ALL` `REVOKE ALL`
TIPOS DE AUTENTICACIÓN ADEMÁS DE CONTRASEÑA

CREATE ROLE IF NOT EXISTS usuario_lector;


`mysql --user=root --password -p -h  127.0.0.1 -P33006`

# Lista de usuarios

`SELECT User FROM mysql.user;`
```
+------------------+
| User             |
+------------------+
| dbuser           |
| root             |
| mysql.infoschema |
| mysql.session    |
| mysql.sys        |
| root             |
+------------------+
```

`SHOW PRIVILEGES;`




SHOW COLUMNS FROM mysql.user;

SELECT Host, User FROM mysql.user;


Cómo registrar nuevos usuarios, modificarlos y eliminarlos.
Cómo se autentican los usuarios y qué opciones de autenticación nos ofrece el SGBD.
Cómo mostrar los usuarios existentes y sus permisos.
Qué permisos puede tener un usuario y qué nivel de granularidad nos ofrece el SGBD.
Qué permisos hacen falta para gestionar los usuarios y sus permisos.
Posibilidad de agrupar los usuarios (grupos/roles/...).
Qué comandos usamos para gestionar los usuarios y sus permisos.