# Definició i Analisi Estructura de dades exercici "Reservas"

Miquel Real, DAM 1er, La Sènia

Codi complet publicat a:
[Avions!!](https://github.com/MiquelRR/dddbb-acces)

## Aquestos son el requisits Originals del execici:

>### Ejercicio. Reserva de vuelos
>Creamos una Base de datos de un sistema de reservas de vuelos:
>
> - Tabla "Vuelos" con columnas: id_vuelo, origen, destino, fecha, capacidad.
> - Tabla "Pasajeros" con columnas: id_pasajero, número_pasaporte, nombre_pasajero
> - Tabla "Vuelos_Pasajeros" con columnas: id_vuelo, id_pasajero, n_asiento.
>
>En esta base de datos, la tabla "Vuelos_Pasajeros" se utiliza para relacionar las tablas "Vuelos" y "Pasajeros" ya que un pasajero puede reservar muchos vuelos y un vuelo puede tener muchos pasajeros. Se guarda el n_asiento para cada vuelo.Creamos un programa de gestión de reservas. Crea un menú para:*
>1. Alta Vuelo
>2. Alta Pasajero
>3. Reserva Vuelo
>4. Modificar reserva
>5. Baja reserva
>6. Salir
>
>Crea la BD vacia.
>Y toda la gestión vamos a hacerla con nuestro programa.
>Maneja todas la excepciones que se os ocurran.


## Requisits addicionals

Per a donar una miqueta de complexitat i realisme a l'exercici , he decidit afegir :
 - Taula "Aeropuertos" amb informacions reals de destinacions aèries arreu del mon: codigo, nombre pais, ciudad
 - Taula "Tipo_de_Avion" amb informació senzila : id_tipo, modelo, asientos.
 - La taula "Vuelos_Pasajeros" es canvia per "Plazas" afegint Id_plaza, ocupado (enum)


La idea del requisits es la definició del vol:
Un vol te un aeroport d'oritge i un de destinació, es cre a partir dun model d'avio des don hereta el nombre de seients, pero aquesta no es una relació a la bbdd perque queda obert a que es puga modificar els seients del vol sense que afecte al model d'avió. Tambe te una data de vol.

Amb aquesta informació "construim" el planos de l'avio, per a que el passateger puga triar el seient, corredor, finestra, etc .

codi de creació de l'estructura:

Una millora importat que es podria realitzar sobre la operativa del canvi de seien o de la asignació de plaza es podria fer fent servir l'`AUTOCOMMIT` de MYSQL en cas de que hi hagués possibilitat de concurrència.

`AUTOCOMMIT = FALSE`, es una opció que quan està desabilitada, tenim el control de fixar les consultes d'actualització de la bbdd, no es farien efectives fins que fem `COMMIT`

Es podria implementar que no es feren commits fins que es comprobe, per exemple que la resposta del pagament des de una API externa fos ok, per a fer un canvi de seient o qualsevol modificació en la BBDD que fos atòmica, poguent retornar al estat anterior en cas de error amb `ROLLBACK`.



Per a crear una BBDD
```sql
CREATE DATABASE IF NOT EXISTS Reservas;
```
o 
```sql
CREATE SCHEMA  Reservas;
```

Alta de taules:
```sql
CREATE TABLE IF NOT EXISTS Pasajeros (
    numero_pasaporte VARCHAR(20) PRIMARY KEY,
    nombre_pasajero VARCHAR(100)
);
#el numero de passaport o document actua com a clau principal

CREATE TABLE IF NOT EXISTS Aeropuertos (
    Codigo VARCHAR(5) PRIMARY KEY,
    Nombre VARCHAR(30),
    Pais VARCHAR(25),
    Ciudad VARCHAR(25)
);
# el codi internacional de l aerport es la clau

CREATE TABLE IF NOT EXISTS Tipo_de_Avion (
    Id_tipo INT AUTO_INCREMENT PRIMARY KEY,
    modelo VARCHAR(100),
    asientos INT
);

#tenim una clau autoincremental

CREATE TABLE IF NOT EXISTS Vuelos (
    id_vuelo INT AUTO_INCREMENT PRIMARY KEY,
    origen VARCHAR(5),
    destino VARCHAR(5),
    fecha DATE,
    FOREIGN KEY (origen) REFERENCES Aeropuertos(Codigo),
    FOREIGN KEY (destino) REFERENCES Aeropuertos(Codigo)
);
#fem claus forasteres origen i destino , que han de ser tipus compatibles, en aquest cas el mateix tipus exacte.

CREATE TABLE IF NOT EXISTS Plazas (
    Id_plaza INT AUTO_INCREMENT PRIMARY KEY,
    id_asiento VARCHAR(5),
    id_pasajero VARCHAR(20),
    id_vuelo INT,
    ocupado ENUM('si', 'no') DEFAULT 'no',
    FOREIGN KEY (id_vuelo) REFERENCES Vuelos(id_vuelo),
    FOREIGN KEY (id_pasajero) REFERENCES Pasajeros(numero_pasaporte)
);
#referenciem plaza i el pasatger, creem una camp pseudo-boleà per a controlar en un moment donat la reserva con una operació atòmica en cas de tindre concurrència o serveis amb possibilitat de falla com pagaments o autoritzacions

```
Despres de creada una BBDD es pot aterar l'estructura, per exemple per afegir seguretat:
```sql
ALTER TABLE Vuelos
ADD CONSTRAINT chk_origen_destino CHECK (origen <> destino)

```
posem una restriccio per a evitar  vols circulars

![alt text](img/model_db_reservas.svg)

A la carpeta QUERIES estan les consultes que se han fet servir per al desnvolupament del exercici.

Tot l'acces a la bbdd es fa desde la mateixa classe de Java, on estan enmagatzemades conm a String les consultes que es poden necessitar, parametrizables des de el programa principal

```java
import java.sql.*;
[..]
public class Accesdb {
    private final static String host="jdbc:mysql://localhost:3306/"; //local
    //private final static String host="jdbc:mysql://localhost:33006/"; //docker
    private final static String bdcon = host+"Reservas";
    //private final static String bdcon = "jdbc:mysql://localhost:33006/Reservas";
    private final static String us = "root";
    private final static String pw = "root";
    public final static String paisos="SELECT DISTINCT Pais FROM Aeropuertos ORDER BY Pais ASC;";
    public final static String prouCapacQuery="SELECT * FROM Tipo_de_Avion WHERE asientos >= %d ORDER BY asientos LIMIT 1;";
    public final static String aeroportsDe="SELECT * FROM Aeropuertos WHERE Pais = '%s';";
    public final static String paisdecCodi="SELECT Pais FROM Aeropuertos WHERE Codigo='%s'";
    public final static String placesVol="SELECT * FROM Plazas WHERE id_vuelo = %s;";
    public final static String ocupa="UPDATE Plazas SET id_pasajero = '%s', ocupado = 'si' WHERE id_vuelo = %s AND id_asiento = '%s';";
    public final static String contaVolsPas="SELECT COUNT(*) FROM Plazas WHERE id_pasajero = '%s';";
    public final static String volsPas="SELECT Vuelos.id_vuelo FROM Vuelos INNER JOIN Plazas ON Vuelos.id_vuelo = Plazas.id_vuelo WHERE Plazas.id_pasajero = '%s';";
    public final static String volsNoPas="SELECT Vuelos.* FROM Vuelos LEFT JOIN Plazas ON Vuelos.id_vuelo = Plazas.id_vuelo AND Plazas.id_pasajero = '%s' WHERE Plazas.Id_plaza IS NULL";
    public final static String passatgersActius="SELECT DISTINCT Pasajeros.* FROM Pasajeros INNER JOIN Plazas ON Pasajeros.numero_pasaporte = Plazas.id_pasajero;";
    public final static String pasatger="SELECT numero_pasaporte,nombre_pasajero FROM Pasajeros WHERE numero_pasaporte='%s';";
    public final static String reservesPas="SELECT Vuelos.id_vuelo, Vuelos.origen, aeropuerto_origen.Pais AS pais_origen, Vuelos.destino, aeropuerto_destino.Pais AS pais_destino, Vuelos.fecha, Plazas.id_asiento, Plazas.id_plaza, Plazas.id_pasajero FROM Vuelos INNER JOIN Plazas ON Vuelos.id_vuelo = Plazas.id_vuelo INNER JOIN Aeropuertos AS aeropuerto_destino ON Vuelos.destino = aeropuerto_destino.Codigo INNER JOIN Aeropuertos AS aeropuerto_origen ON Vuelos.origen = aeropuerto_origen.Codigo WHERE Plazas.id_pasajero = '%s' ";
    public final static String volsambPlazaLLiure="SELECT DISTINCT id_vuelo FROM Plazas WHERE ocupado = 'no'";
    public final static String lliures="SELECT COUNT(*) FROM Plazas WHERE id_vuelo = %d AND ocupado = 'no'";
    public final static String llibera="UPDATE Plazas SET id_pasajero = null, ocupado='no' WHERE id_plaza = %s;";
    public static Scanner sc = new Scanner(System.in);

[...]
}
```

Podem observar que a mes de fer consultes unitaries hi han funcions que retornen List<String> des d'una taula o una consulta, peer a poder fer el tractament eficient des de el programa principal.

Tambe hem pensat en retornar en les funcions d'inserció la clau autonumérica que es generada per el SGBD per a poder-ne fer ús

Es una classe pensada no sol per a aquest exercici sinó per a re-aprofitar-la.

Aquesta mateixa s'ha fet servir a l'exercici de bases de dades de contestar el test.


```

### Mostrar información base de datos
Con `SHOW` podemos mostrar información, en este caso muestra las bases de datos del sistema.
```sql
SHOW DATABASES;
```

Mientras tanto en este caso muestra el nombre de las tablas de la base de datos.
```sql
SHOW TABLES;
```

Y para concluir vemos como mostrar más en específico los datos de cada columna como puede ser el tipo de datos de cada columna de una tabla.
```sql
SHOW COLUMNS FROM Pasajeros;
```

### Inserción de datos
Se ha pedido a  *ChatGPT*<sub>[[1](#referencias)]</sub> que genere una serie de `INSERT`s para facilitar el proceso de creación de la base de datos:
```sql
-- Insertar datos de ejemplo en la tabla Pasajeros
INSERT INTO Pasajeros (pasaporte, nombre) VALUES
('ABC123', 'Juan Pérez'),
('DEF456', 'María López'),
('GHI789', 'Luis García');

-- Insertar datos de ejemplo en la tabla Vuelos
INSERT INTO Vuelos (id_Vuelo, origen, destino, fecha, capacidad) VALUES
('VUELO1', 'Madrid', 'Barcelona', '2024-04-01 08:00:00', 200),
('VUELO2', 'Barcelona', 'Madrid', '2024-04-02 10:00:00', 180),
('VUELO3', 'Sevilla', 'Valencia', '2024-04-03 12:00:00', 150);

-- Insertar datos de ejemplo en la tabla Reservas
INSERT INTO Reservas (id_Reserva, pasaporte, id_vuelo, asiento) VALUES
('RESERVA1', 'ABC123', 'VUELO1', 'A1'),
('RESERVA2', 'DEF456', 'VUELO2', 'B3'),
('RESERVA3', 'GHI789', 'VUELO3', 'C2');
```
En los `INSERT INTO` podemos ver como se especifica cada tabla y sus valores para que no haya ningún tipo de error detrás de `VALUE` entre paréntesis.
# Notas tomadas en clase
### IMPORTANTE
Cuando desarrollamos nunca empezamos de 0. siempre hay una capa por debajo, ojo, hay que conocer esa capa. Es decir, hay dedicar días para leer documentación en nuevos proyectos para evitar problemas futuros y malgasto de tiempo.

### Curiosidades
Se puede hacer una **subconsulta** para conseguir un dato de otra tabla de la siguiente manera:
```sql
DELETE FROM Reservas
WHERE pasaporte IN (
    SELECT pasaporte FROM Pasajeros
    WHERE nombre LIKE "Elsa%");
```
Es como hacer un `SELECT` dentro de un `DELETE FROM` para especificar (en este caso) los nombres que empiecen por *Elsa*.

`AUTOCOMMIT` tiene que ver con las transacciones. Esta opción sirve para que se apliquen los cambios con cada transacción/ejecución de una query, nos ayuda a no perder los datos al cerrar el programa.  
Para activarlo simplemente hay que ejecutar el siguiente código SQL:
```sql
SET AUTOCOMMIT = TRUE; -- FALSE Si no lo queremos
```
Si por el contrario queremos tener un control del progreso de la base de datos y que no se guarden los cambios cada vez que queremos realizar una transacción podemos hacerlo ejecutando el comando `COMMIT` cada vez que queramos aplicar los cambios:
```sql
COMMIT;
```
 Si queremos volver al estado en el momento del 'commiteo' ejecutamos `ROLLBACK`
```sql
ROLLBACK;
```
Obviamente deberemos desactivar el `AUTOCOMMIT` cuando queramos hacerlo manual.  
aunque Mysql no está orientado a transacciones. Lo permite pero no le pone un nombre a la transacción.  

El siguiente comando `SQL` cuenta las reservas de cada vuelo ya que los hemos agrupado con un `GROUP BY`. Esto sirve para (como bien dice el nombre) agrupar los valores del `SELECT`.
```sql
SELECT id_vuelo, COUNT(*)
FROM Reservas
GROUP BY id_vuelo;
```

Se pueden concatenar textos con los valores de los campos usando la función `CONCAT()` de la siguiente manera.
```sql
SELECT 
    *,
    YEAR(fecha),
    CONCAT("Desde: ", origen, " Hasta: ", destino)
FROM Vuelos;
```
# Respuesta a las preguntas
**¿Por qué necesitamos tres tablas?**  
Necesitamos tres tablas ya que como podemos ver en el diseño de la base de datos las tablas *pasajeros* y *vuelos* estás relacionadas de muchos a muchos, esto requiere una tabla externa como es la tabla *reservas*.  

**¿Cuáles son las claves primarias y foráneas?**  
Las claves primarias en una tabla son una forma de identificar cada registro de la tabla, los valores del campo donde esté asignada la clave no se pueden repetir. Por otro lado una clave foránea es una clave primaria de otra tabla, se usa para relacionar los datos y evitar datos erroneos como (en este caso) vuelos o pasajeros que no existan. 

**¿Por qué utilizamos las restricciones que hemos definido y no otras?**  
Hemos utilizado las restricciones `NOT NULL` para evitar valores nulos y que sea necesario rellenar esos campos. Tambíen hemos utilizado `UNSIGNED` en la tabla vuelos por optimización, esto hace que el no tome valores negativos sino que se pasen a positivo.

### Reflexiones
Para cancelar un vuelo sería necesario borrarlo de la siguiente manera:
```sql
DELETE FROM Vuelos WHERE id_Vuelo LIKE "VUELO1";
```
En este caso intentamos borrar el vuelo con id *VUELO1*. Sin embargo nos salta un error (Error: *1451*<sub>[[3](#referencias)]</sub>) que nos dice que no se puede eliminar el regitro ya que se está utilizando los datos de este registro en otra tabla (*reservas*). Si queremos borrar el registro debemos asegurarnos de que no se usa nada de información en tablas exteriores, en este caso sería borrar las reservas de ese vuelo. Se pueden denotar las ventajas e inconvenientes de las bases de datos relacionales: En primer lugar vemos que gracias a las bases de datos relacionales al relacionar la información evitamos inconcluencias en los datos almacenados y facilitamos la búsqueda de registros entre tablas. Por otro lado nos restringe a la hora de modificar o eliminar registros, ya que no nos deja eliminar un registro si está en uso en otra tabla a través de una c

## Captures de pantalla

![alt text](<img/Captura desde 2024-04-02 14-58-32.png>)
![alt text](<img/Captura desde 2024-04-02 14-59-30.png>)
![alt text](<img/Captura desde 2024-04-02 15-00-53.png>)