### Query 11 
Muestra qué clientes han realizado compras por valores superiores a 10€, ordenados por apellido.

```sql
USE Chinook;
SELECT Customer.FirstName,
       Customer.LastName,
       SUM(Invoice.Total) AS SumaFacturas
FROM Customer
JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId
GROUP BY Customer.CustomerId
HAVING SumaFacturas > 10
ORDER BY Customer.LastName;
```
Para que operen las funciones de Agregacion es necesario que hayan grupos definidos con GROUP BY

HAVING aplica condiciones sobre agregaciones, como SUM, COUNT, etc.

en este caso al usar  GROUP BY indicamos un campo  y agrupa por los elementos que se repiten. Es la unica manera de usar GROUP BY ?

No, el uso de GROUP BY en SQL no se limita a agrupar por un solo campo. Puedes agrupar por múltiples campos según tus necesidades.

Por ejemplo, supongamos que tienes una tabla de ventas con información sobre el producto vendido y la región donde se realizó la venta. Puedes querer agrupar tus datos por ambos campos para obtener la suma de las ventas por combinación de producto y región.

La sintaxis sería la siguiente:

```sql
SELECT Producto, Region, SUM(Venta) AS TotalVenta
FROM Ventas
GROUP BY Producto, Region;
```

En este caso, la cláusula GROUP BY agrupa los datos según los valores de las columnas Producto y Region, lo que resulta en un conjunto de resultados donde cada fila representa una combinación única de producto y región. Luego, SUM(Venta) calcula la suma de las ventas para cada combinación única de producto y región.